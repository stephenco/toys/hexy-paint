# Purpose

Forked from the quick and dirty original Hexy project. Experiment with painting
properties over a field of hexagons as a POC for starting a map builder.

## Usage

`npm run dev`

Hosted at localhost:8080 by default.
Hover over spaces and paint areas by clicking and dragging the mouse around.
Select different paint colors by clicking on color tiles in the sidebar on the
left of the page.

Additional console commands can be made to interact with the `world` object.
You can `world.export()` to get a simplified representation of the world that
can be passed to other code, and you can `world.import( data )` to repaint the
world given new information (from a previous export). Imported hexes are applied
to the middle of the painting, and you can also use `world.center()` to combine
an export and an import operation in one statement. Results are rendered to
the browser script console.

## Issues

Hexy was quick and dirty code to begin with, and I just took that and ran with
it to chop out and extend behavior as needed. There is too much code around
hex location calculation and a lot of the workd done in here if fairly ugly. The
experience was educational though, and it serves its purpose as a POC.